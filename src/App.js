import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { Button, Table, Alert, Container, Row, Col, Form } from "react-bootstrap/";

class App extends Component {
  constructor() {
    super();
    this.state = {
      firstname: "",
      lastname:"",
      email: "",
      selectedDesignation: "",
      selectedTiming: "",
      location: "",
      defaultDesignation: [
        { value: "Select Designation", label: "null" },
        { value: "Training", label: "Training"},
        { value: "Intern", label: "Intern" },
        { value: "Junior", label: "Junior" },
        { value: "Mid", label: "Mid" },
        { value: "Leader", label: "Leader" },
        { value: "Senior", label: "Senior" }
      ],
      defaultTiming: [
        { value: "Select Timing" },
        { value: "6-2" },
        { value: "8-4" },
        { value: "9-6" },
        { value: "10-6" },
        { value: "11-5"},
        { value: "12-6"}
      ],
      employee: [],
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit = async e => {
    e.preventDefault();
    if (!this.isEmpty()) {
      const {
        selectedDesignation,
        selectedTiming,
        firstname,
        lastname,
        email,
        location
      } = this.state;
      const newEmployee = {
        firstname: firstname,
        lastname: lastname,
        email: email,
        location: location,
        timing: selectedTiming,
        designation: selectedDesignation
      };
      console.log("newEmployee", newEmployee);
      if (this.isUnique(email)) {
        await this.setState(preState => {
          return {
            firstname: "",
            lastname: "",
            email: "",
            location: "",
            timing: "",
            designation: "",
            message: "",
            employee: preState.employee.concat(newEmployee)
          };
        });
        console.log("newSateEmployee", this.state.employee);
      } else {
        this.setState({ message: "Email must be Unique" });
        // console.log(this.state.message);
      }
    }
  };
  isEmpty() {
    let message = "";
    if (this.state.firstname === "") {
      message = " First Name cannot be Empty";
    } else if (this.state.lastname === "") {
      message = "Last Name cannot be empty";
    } else if (this.state.email === "") {
      message = "Email cannot be empty";
    } else if (this.state.location === "") {
      message = "Location cannot be Empty";
    } else if (this.state.selectedDesignation === "") {
      message = "selectedDesignation cannot be empty";
    } else if (this.state.selectedTiming === "") {
      message = "selectedTiming cannot be empty";
    } else {
      message = "";
    }
    this.setState({ message });
    if (message === "") return false;
    return true;
  }
  isUnique(newEmail) {
    const found = this.state.employee.find(item => item.email === newEmail);
    return typeof found === "undefined" ? true : false;
  }
  handleChange = e => {
    const { id, value } = e.target;
    this.setState({
      [id]: value
    });
  };

  render() {
    const defaultDesignation = this.state.defaultDesignation;
    const defaultTiming = this.state.defaultTiming;
    const employees = this.state.employee;
    const message = this.state.message;

    return (
      <Container fluid>
        <header className="App-header"><b>Employee Form</b></header>
        <br />
        <Form>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridFirstName">
              <Form.Label>Given Name:</Form.Label>
              <Form.Control 
                  type="firstname"
                  value={this.state.firstname}
                  onChange={this.handleChange}
                  name="firstname"
                  id="firstname"
                  placeholder=" Enter your first name..." 
              />
            </Form.Group>

            <Form.Group as={Col} controlId="formGridLastName">
              <Form.Label>Family Name :</Form.Label>
              <Form.Control 
                  type="lastname"
                  value={this.state.lastname}
                  onChange={this.handleChange}
                  name="lastname"
                  id="lastname"
                  placeholder=" Enter your last name..." 
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridlevel">
              <Form.Label>Selected Designation :</Form.Label>
                <Form.Control
                  as="select"
                  id="selectedDesignation"
                  onChange={this.handleChange}
                >
                  {defaultDesignation.map((item, index) => (
                    <option key={index}>{item.value}</option>
                  ))}
                  >
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col} controlId="formGridEmail">
                <Form.Label>Email Address :</Form.Label>
                  <Form.Control
                    type="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    name="email"
                    id="email"
                    placeholder=" Enter your email address..."
                  />
                  <Form.Text className="text-muted">
                    <strong>Note :</strong> We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>
            </Form.Row>
            <Form.Row>
            <Form.Group as={Col} controlId="formGridLocation">
              <Form.Label>Location :</Form.Label>
                <Form.Control
                  type="text"
                  value={this.state.location}
                  onChange={this.handleChange}
                  name="location"
                  id="location"
                  label="location"
                  placeholder="Enter your full address..."
                />
              </Form.Group>
          
            <Form.Group as={Col} controlId="formGridtime">
              <Form.Label>Selected Timing:</Form.Label>
                <Form.Control
                  as="select"
                  id="selectedTiming"
                  onChange={this.handleChange}
                >
                  {defaultTiming.map((item, index) => (
                    <option key={index}>{item.value}</option>
                  ))}
                  >
                </Form.Control>
              </Form.Group>
            </Form.Row>
        
            <Col>
              <Button
                variant="primary"
                style={{ marginTop: "0px" }}
                type="submit"
                onClick={this.onSubmit}

              >
                Submit form
              </Button>
            </Col>
          
        </Form>
        <Row>
          {" "}
          {message === "" ? (
            ""
          ) : (
            <Alert variant="danger">Oh snap! you got an Error: {message}</Alert>
          )}
        </Row>

        {this.state.employee.length === 0 ? (
          ""
        ) : (
          <div>
            <hr />
            <Row>
              <Col>
              <label><b>Lists Of Employes: </b></label>
              </Col>
            </Row>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name </th>
                  <th>Email Address</th>
                  <th>Location</th>
                  <th>Designation</th>
                  <th>Timing</th>
                </tr>
              </thead>
              <tbody>
                {employees.map((employee, index) => (
                  <tr horizontal key={index}>
                    <td> {index + 1} </td>
                    <td> {employee.firstname +" "+ employee.lastname} </td>
                    <td> {employee.email}</td>
                    <td> {employee.location}</td>
                    <td> {employee.designation}</td>
                    <td> {employee.timing}</td>{" "}
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </Container>
    );
  }
}

export default App;